api = 2
core = 7.x

;****************************************
; Stub Makefiles
;****************************************

;includes[] = adapted-modules.make

includes[] = adapted-panopoly.make
;includes[] = http://drupalcode.org/project/panopoly.git/blob/799304e96ba9dfb93f3dec20a9ce95e3df98e75b:/drupal-org.make

;includes[] = adapted-themes.make

;includes[] = adapted-libraries.make

;****************************************
; Installation Requirements - these must be present before installation process begins
;****************************************

projects[breakpoints][type] = module
projects[breakpoints][subdir] = contrib

projects[radix][type] = theme
projects[radix][download][type] = git
projects[radix][download][branch] = 7.x-2.x

projects[sizzle][type] = theme
projects[sizzle][download][type] = git
projects[sizzle][download][branch] = 7.x-1.x

projects[superfish][type] = module
projects[superfish][version] = 1.x-dev
projects[superfish][subdir] = contrib

;****************************************
; Go! Go! Gadget Go !
;****************************************
