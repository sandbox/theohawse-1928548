api = 2
core = 7.x

; Core
includes[] = http://drupalcode.org/sandbox/theohawse/1928548.git/blob_plain/HEAD:/drupal-org-core.make

; Profile
projects[adapted][type] = profile
projects[adapted][download][type] = git
projects[adapted][download][branch] = 7.x-1.x
projects[adapted][download][url] = http://git.drupal.org/sandbox/theohawse/1928548.git

; Dependencies
includes[] = http://drupalcode.org/sandbox/theohawse/1928548.git/blob_plain/HEAD:/adapted-modules.make
includes[] = http://drupalcode.org/sandbox/theohawse/1928548.git/blob_plain/HEAD:/adapted-themes.make
includes[] = http://drupalcode.org/sandbox/theohawse/1928548.git/blob_plain/HEAD:/adapted-libraries.make
;includes[] = http://drupalcode.org/project/panopoly.git/blob/799304e96ba9dfb93f3dec20a9ce95e3df98e75b:/drupal-org.make
